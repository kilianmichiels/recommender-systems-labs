# Recommender Systems Labs

Aanbevelingssystemen zijn technieken en algoritmen die suggesties genereren voor
content op basis van de persoonlijke interesses van de gebruiker. Deze worden typisch
gebruikt als een oplossing voor de overvloed aan informatie, die op vele online diensten
beschikbaar is zoals Amazon, Netflix en Facebook. Het onderzoek rond aanbevelingssystemen
is een relatief nieuw domein, ontstaan uit de kennis rond information retrieval,
machinaal leren en data mining. Net zoals bij zoekmachines zal er informatie of
content opgehaald worden (information retrieval) en aangeboden worden aan de eindgebruiker.
In tegenstelling tot zoekmachines, die werken met ingevoerde zoektermen, zullen
aanbevelingssystemen de content selecteren op basis van persoonlijke interesses.
Een aanbevelingssysteem zal deze persoonlijke interesses automatisch aanleren
(machinaal leren) op basis van het gedrag en de interactie van de gebruiker met
de dienst en eventuele feedback. Hiervoor wordt vaak gebruik gemaakt van grote
hoeveelheden historische data waarin bepaalde patronen geïdentificeerd worden,
en zo voorkeuren geleerd worden (datamining).

## Lab 01: Non-Personalised Recommender

The goal is to develop a non-personalised recommender based on product association rules.
You will have to write the source code for calculating these recommendations.

## Lab 02: Recommendations as a Service

The goal is to interact with a recommendation service.
You will write source code to submit a data set to a recommendation web service.
You will run calculation jobs through a dashboard user interface,
and you will write code to query the web service for personalised recommendations.

This lab requires to install the [Suggestio library](https://github.com/sugestio/sugestio-python).

## Lab 03: Personalised Recommendations

The goal is to develop a set of personalised recommendation algorithms and run these to generate recommendations.
You will have to write the source code for calculating these recommendations.
Make your algorithm efficient and structured. Think about an appropriate class structure.
You have to implement the algorithms yourself.
Do not use an existing software package or library for calculating recommendations.
However, you can use more general, existing libraries for general tasks such as data storage.
