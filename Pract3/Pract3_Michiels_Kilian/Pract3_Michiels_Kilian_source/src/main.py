#!/usr/bin/env python
""" Goal: The goal is to develop a set of personalized recommendation
    algorithms and run these to generate recommendations.
"""
# Imports
from arguments import *
from manager import Manager
from librarian import Librarian
from analyst import Analyst

# Authorship Information
__author__ = "Kilian Michiels"
__email__ = "Kilian.Michiels@UGent.be"
__date__ = "31/03/2019"


def main():
    args = fetch_args()
    manager = Manager(args)
    if args['run_all']:
        manager.run_all()
    elif args['q'] > 0:
        manager.run_question(int(args['q']))
    # Always ask for more question numbers.
    manager.run_main_loop()


if __name__ == '__main__':
    main()
