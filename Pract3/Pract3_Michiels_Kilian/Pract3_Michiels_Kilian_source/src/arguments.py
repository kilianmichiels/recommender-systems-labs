#!/usr/bin/env python
""" Argument Parser for the application.
"""

# Imports
import argparse

# Authorship Information
__author__ = "Kilian Michiels"
__email__ = "Kilian.Michiels@UGent.be"
__date__ = "31/03/2019"


def fetch_args():
    # Create parser
    parser = argparse.ArgumentParser(
        description="Recommender Systems - Practical Exercise 3: \
        Personalized Recommendations.",
        formatter_class=argparse.RawTextHelpFormatter)

    # Arguments
    parser.add_argument('--run_all', action='store_true', default=False,
                        help="Run main with all commands from PDF.")

    parser.add_argument('-q', type=int, default=0,
                        help="Number of the question in the analysis to run.")

    args = vars(parser.parse_args())

    return args
