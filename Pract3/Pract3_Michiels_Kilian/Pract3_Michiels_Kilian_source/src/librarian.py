#!/usr/bin/env python
""" Librarian handles all data read/write operations.

Fetch data, convert data into the correct format, store data,...
"""

# Imports
import pandas as pd
import numpy as np
from tqdm import tqdm

# Authorship Information
__author__ = "Kilian Michiels"
__email__ = "Kilian.Michiels@UGent.be"
__date__ = "31/03/2019"


class Librarian(object):

    def __init__(self, args=None):
        super(Librarian, self).__init__()
        self.args = args

        self.library = dict()

        # File locations:
        self.ratings_location = "../data/ratings.csv"
        self.movies_location = "../data/movies.csv"

        # Basic set up
        self.read_data()

    def read_data(self):
        """
        Read CSV files and convert them to a DataFrame.
        """
        tqdm.write('Librarian - Reading and processing .csv files...')
        self.library['ratings'] = pd.read_csv(self.ratings_location,
                                              dtype={'userId': int,
                                                     'movieId': int,
                                                     'rating': float,
                                                     'timestamp': int})
        self.library['ratings'].columns = ['UserID', 'MovieID', 'Rating',
                                           'Timestamp']

        # Convert string to numeric
        for col in self.library['ratings'].columns:
            self.library['ratings'][col] = pd.to_numeric(
                self.library['ratings'][col])

        self.library['movies'] = pd.read_csv(self.movies_location,
                                             dtype={'movieId': int,
                                                    'title': str,
                                                    'genres': str})
        self.library['movies'].columns = ['MovieID', 'Title', 'Genres']

        # Convert string to numeric
        self.library['movies']['MovieID'] = pd.to_numeric(
            self.library['movies']['MovieID'])

        # Create an extra library entry to simplify the creation of the
        # Item-Item Matrix
        rating_df = self.library['ratings']
        num_movies = self.library['movies'].MovieID.unique()
        num_users = self.library['ratings'].UserID.unique()
        user_watched_table = np.full((len(num_movies), len(num_users)),
                                     np.nan)
        movie_id_map = dict()
        id_movie_map = dict()

        for i, m_id in tqdm(enumerate(num_movies),
                            total=len(num_movies), leave=False):
            # Fetch movie ratings
            movie_id_map[i] = m_id
            id_movie_map[m_id] = i
            df = rating_df[rating_df['MovieID'] == m_id][['UserID', 'Rating']]
            for index, row in df.iterrows():
                user_watched_table[i, int(row.UserID) - 1] = row.Rating

        self.library['movie_id_map'] = movie_id_map
        self.library['id_movie_map'] = id_movie_map
        self.library['non_centered'] = user_watched_table

        # Make it Item-Mean Centered.
        means = np.nanmean(user_watched_table, axis=1, keepdims=True)
        item_mean_centered = user_watched_table - means
        self.library['item_mean_centered'] = item_mean_centered

        # Make it User-Mean Centered.
        means = np.nanmean(user_watched_table, axis=0, keepdims=True)
        user_mean_centered = user_watched_table - means
        self.library['user_mean_centered'] = user_mean_centered

        tqdm.write('Librarian - Files from disk processed...')
