#!/usr/bin/env python
""" Analyst performs calculations on the data.
"""

# Imports
import pandas as pd
from tqdm import tqdm
import numpy as np
import parmap
from time import time
from multiprocessing import cpu_count
from math import factorial
from itertools import combinations

# Authorship Information
__author__ = "Kilian Michiels"
__email__ = "Kilian.Michiels@UGent.be"
__date__ = "31/03/2019"


class Analyst(object):

    def __init__(self, lib):
        super(Analyst, self).__init__()
        self.library = lib
        self.ii_matrix = None
        self.check_ii_matrix_presence()

    ##########################################################################
    #                       USER-USER COLLABORATIVE FILTERING                #
    ##########################################################################

    def recommend_uucf(self, user_id, n=10):
        tqdm.write('Analyst - Recommending Top N with UUCF...')
        unrated_ids = self.fetch_unrated_movies(user_id)

        tqdm.write('Analyst - Number of unrated movies: {}'
                   .format(len(unrated_ids)))

        # Calculate all the assocations
        func_args = [(user_id, movie_id) for movie_id in unrated_ids]

        results = []
        for f1, f2 in tqdm(func_args, desc='Calculating User-User CF',
                           leave=False):
            results.append(self.user_user_cf(f1, f2))

        # Sort on highest association and next on lowest ID
        results = sorted(results, key=lambda e: (e[2], -e[0]), reverse=True)

        r = pd.DataFrame(results[:n])
        r.columns = ['MovieID', 'UserID', 'Prediction', 'Weighted_Avg']
        return r

    def user_user_cf(self, user_id, movie_id):
        """Only neighbors with a positive similarity value...
        """
        # This gives [<UserID>, <Rating>, <Weight>]
        neighbors = self.find_neighbourhood(user_id, movie_id)

        r_a_mean = self.fetch_mean_rating_for_user(user_id)

        if len(neighbors) == 0:
            return [movie_id, user_id, r_a_mean]

        denom = np.sum(neighbors, axis=0)[2]

        if denom == 0:
            return [movie_id, user_id, r_a_mean]

        num = np.sum(np.multiply(neighbors[:, 1], neighbors[:, 2]))

        prediction = r_a_mean + (num / denom)
        return [movie_id, user_id, prediction, (num / denom)]

    def find_neighbourhood(self, user_id, movie_id, k=20):
        """
        Find the top k users with the highest influence on the specified
        user. This highest influence is calculated using the Pearson
        Correlation between the similar rated items.

        We only consider positive similarities.
        """
        user_ids = self.fetch_user_ids(movie_id)
        for i, id_r in enumerate(user_ids):
            user_ids[i].append(
                self.calculate_pearson_correlation(user_id, int(id_r[0])))

        neighbors = [w for w in user_ids if w[2] > 0]

        # Sort, first on highest correlation and next on lowest user ID
        neighbors = sorted(neighbors, key=lambda e: (e[2], -e[0]),
                           reverse=True)
        return np.array(neighbors)[:k]

    ##########################################################################
    #                       ITEM-ITEM COLLABORATIVE FILTERING                #
    ##########################################################################

    def recommend_iicf(self, user_id, n=10):
        tqdm.write('Analyst - Recommending Top N with IICF...')

        unrated_ids = self.fetch_unrated_movies(user_id)

        tqdm.write('Analyst - Number of unrated movies: {}'
                   .format(len(unrated_ids)))

        # Calculate all the assocations
        func_args = [(user_id, movie_id)
                     for movie_id in unrated_ids]

        results = []
        for f1, f2 in tqdm(func_args, desc='Calculating Item-Item CF',
                           leave=False):
            results.append(self.item_item_cf(f1, f2))

        # Sort on association and next on ID (highest first)
        results = sorted(results, key=lambda e: (e[2], -e[0]), reverse=True)

        r = pd.DataFrame(results[:n])
        r.columns = ['MovieID', 'UserID', 'Prediction']

        return r

    def item_item_cf(self, user_id, movie_id, k=20):
        prediction = 0
        similar_movies = self.fetch_similar_rated_movies(user_id, movie_id, k)

        if len(similar_movies) == 0:
            return [movie_id, user_id, prediction]

        # Convert negative numbers to zero
        sims = [s for s in similar_movies if s[2] > 0]

        if len(sims) == 0:
            return [movie_id, user_id, prediction]

        denom = np.sum(np.absolute(sims), axis=0)[2]
        if denom == 0:
            return [movie_id, user_id, prediction]

        sims = np.array(sims)
        num = np.sum(np.multiply(sims[:, 2], sims[:, 1]))

        prediction = 1.0 * num / denom
        return [movie_id, user_id, prediction]

    def construct_item_item_matrix(self):
        distinct_movies = self.fetch_movie_ids()

        num_cpus = cpu_count()
        num_combinations = (int(factorial(len(distinct_movies))
                                / (2 * factorial(len(distinct_movies) - 2))))

        func_args = []

        # for each movie_1 in all movies
        for m1, m2 in tqdm(combinations(distinct_movies, 2),
                           total=num_combinations, leave=False):

            func_args.append((m1, m2))

        tqdm.write('Analyst - Ready to calculate {} similarities.\n'
                   .format(len(func_args)))

        start = time()
        results = parmap.starmap(self.calculate_similarity,
                                 func_args,
                                 pm_pbar=True,
                                 pm_parallel=True,
                                 pm_processes=num_cpus,
                                 pm_chunksize=len(func_args) // num_cpus // 4)

        r = pd.DataFrame(index=distinct_movies, columns=distinct_movies)
        for res in tqdm(results, leave=False):
            r.at[res[0], res[1]] = res[2]
            r.at[res[1], res[0]] = res[2]  # Make symmetric

        # Set the sim(i,i) to 1
        r.values[[np.arange(r.shape[0])] * 2] = 1.0

        tqdm.write('Analyst - Similarities Calculated - '
                   'Processing Time: {:.4f} seconds.'
                   .format(time() - start))

        # Store matrix so we do not have to calculate every time.
        r.to_pickle('../data/ii_matrix.h5')
        return r

    ##########################################################################
    #                           BASKET RECOMMENDATIONS                       #
    ##########################################################################

    def recommend_basket(self, basket_items, n=10, use_negatives=False):
        tqdm.write('Analyst - Recommending Top N with Basket '
                   'Recommendations...')

        if len(basket_items) == 1:
            results = self.fetch_similar_movies(basket_items[0], n)
            r = pd.DataFrame(results)
            r.columns = ['MovieID', 'Score']
            return r

        func_args = [(basket_items, m_id, use_negatives)
                     for m_id in self.fetch_movie_ids()
                     if m_id not in basket_items]  # Remove the items in basket

        results = []
        for f1, f2, f3 in func_args:
            results.append(self.basket_score(f1, f2, f3))

        # Sort on association and next on ID (highest first)
        results = sorted(results, key=lambda e: (e[1], -e[0]), reverse=True)

        r = pd.DataFrame(results[:n])
        r.columns = ['MovieID', 'Score']
        return r

    def basket_score(self, basket_items, movie_id, use_negatives):
        if use_negatives:
            sims = [self.fetch_similarity(i, movie_id) for i in basket_items]
        else:
            sims = [max(0, self.fetch_similarity(i, movie_id))
                    for i in basket_items]
        return [movie_id, sum(sims)]

    ##########################################################################
    #                           HYBRID RECOMMENDATIONS                       #
    ##########################################################################

    def recommend_hybrid(self, user_id, n=10):
        tqdm.write('Analyst - Recommending Top N with Hybrid '
                   'Recommendations...')

        max_num_recoms = len(self.fetch_movie_ids())

        uucf_recoms_df = pd.DataFrame(self.recommend_uucf(user_id,
                                                          max_num_recoms))
        uucf_recoms_df.columns = ['MovieID', 'UserID', 'UUCF_Prediction',
                                  'Weighted_Avg']
        iicf_recoms_df = pd.DataFrame(self.recommend_iicf(user_id,
                                                          max_num_recoms))
        iicf_recoms_df.columns = ['MovieID', 'UserID', 'IICF_Prediction']

        uucf_recoms_df.drop(['UserID'], axis=1, inplace=True)
        uucf_recoms_df.drop(['Weighted_Avg'], axis=1, inplace=True)
        iicf_recoms_df.drop(['UserID'], axis=1, inplace=True)

        hybrid_recoms_df = pd.merge(
            uucf_recoms_df,
            iicf_recoms_df,
            how='inner',
            on='MovieID'
        )

        def calculate_new_prediction_score(row):
            row['Hybrid_Prediction'] = ((row['UUCF_Prediction'] * 0.5)
                                        + (row['IICF_Prediction'] * 0.5))
            return row

        hybrid_recoms_df = hybrid_recoms_df.apply(
            calculate_new_prediction_score, axis=1)

        return hybrid_recoms_df.nlargest(n, 'Hybrid_Prediction')

    ##########################################################################
    #                                    MATH                                #
    ##########################################################################

    def calculate_pearson_correlation(self, user_1, user_2, sig_w=True,
                                      cut_val=10):
        """
        Calculate the Pearson Correlation between user_1 and user_2 for items
        that they have both rated.
        """
        common_r = np.array(self.fetch_common_user_ratings(
            user_1, user_2, 'user_mean_centered'))

        # If the users have 0 or 1 items in common.
        if len(common_r) <= 1:
            return 0

        denom = (np.sqrt(np.nansum(np.square(common_r[:, 0])))
                 * np.sqrt(np.nansum(np.square(common_r[:, 1]))))

        # Prevent division by zero
        if denom == 0:
            return 0

        num = np.sum([(r1 * r2) for r1, r2 in common_r])

        w = 1.0 * num / denom
        if sig_w:
            return w * (min(cut_val, len(common_r)) / cut_val)
        else:
            return w

    def calculate_similarity(self, movie_1, movie_2):
        common = self.fetch_common_user_ratings(movie_1, movie_2,
                                                'item_mean_centered')

        if len(common) == 0:
            return [movie_1, movie_2, 0]

        r_1_norm = self.fetch_movie_ratings(movie_1)
        r_2_norm = self.fetch_movie_ratings(movie_2)

        # Denominator calculates the norm of the vectors. The sum is over all
        # users who rated the item.
        denom = (np.sqrt(np.nansum(np.square(r_1_norm)))
                 * np.sqrt(np.nansum(np.square(r_2_norm))))

        # Prevent division by zero
        if denom == 0:
            return [movie_1, movie_2, 0]
        num = np.sum([(r1 * r2) for r1, r2 in common])
        similarity = (1.0 * num / denom).astype(float)

        return [movie_1, movie_2, similarity]

    ##########################################################################
    #                                   HELPERS                              #
    ##########################################################################

    def check_ii_matrix_presence(self):
        if self.ii_matrix is None:
            try:
                tqdm.write('Analyst - Reading the item-item matrix '
                           'from file...')
                filename = '../data/ii_matrix.h5'
                self.ii_matrix = pd.read_pickle(filename).to_numpy()

            except Exception as e:
                tqdm.write('Analyst - Could not read from file, '
                           'creating new item-item matrix...')
                self.ii_matrix = self.construct_item_item_matrix().to_numpy()

    def fetch_user_ratings(self, userId):
        """Fetch all ratings for a specified user.
        """
        mur = self.library['non_centered']
        mmap = self.library['movie_id_map']
        ratings = [(mmap[i], mur[i, userId - 1]) for i, r
                   in enumerate(np.isnan(mur[:, userId - 1]))
                   if not r]
        return ratings

    def fetch_common_user_ratings(self, id_1, id_2, key):
        """Fetch all common ratings between two given ID's.
        """
        mur = self.library[key]
        if key == 'user_mean_centered':
            ratings_1 = mur[:, id_1 - 1]
            ratings_2 = mur[:, id_2 - 1]
        else:
            mmap = self.library['id_movie_map']
            ratings_1 = mur[mmap[id_1], :]
            ratings_2 = mur[mmap[id_2], :]

        mmap = self.library['movie_id_map']
        common = ([ratings_1[i], ratings_2[i]]
                  for i, (r1, r2) in enumerate(
                      zip(np.isnan(ratings_1),
                          np.isnan(ratings_2)))
                  if not r1 and not r2)

        return np.array(list(common))

    def fetch_mean_rating_for_user(self, userId):
        """Fetch the mean rating for a given user ID.
        """
        return np.nanmean(self.library['non_centered'][:, userId - 1], axis=0)

    def fetch_similar_rated_movies(self, userId, movieId, k=20):
        """Fetch top K similar movies according to the similarity matrix.
        These similar movies must be common with the rated movies by the user.
        """
        rated_movies = self.fetch_user_ratings(userId)
        mmap = self.library['id_movie_map']
        movie_col = self.ii_matrix[mmap[movieId], :]
        common = []
        for i, (m_id, r) in enumerate(rated_movies):
            if m_id != movieId:
                common.append([m_id, r, movie_col[mmap[m_id]]])

        # Sort on similarity and next on ID (highest first)
        res = sorted(common, key=lambda e: (e[2], -e[0]), reverse=True)
        return res[:k]

    def fetch_movie_ratings(self, movieId):
        """Fetch all the ratings for a given movie ID.
        """
        mmap = self.library['id_movie_map']
        return self.library['item_mean_centered'][mmap[movieId], :]

    def fetch_unrated_movies(self, userId, user_centered=True):
        """Fetch all the items that have not yet been rated by a given user ID.
        """
        mur = self.library['user_mean_centered']
        mmap = self.library['movie_id_map']
        user_ratings = np.isnan(mur[:, userId - 1])
        unrated = [mmap[i] for i, r in enumerate(user_ratings) if r]
        return unrated

    def fetch_movie_ids(self):
        """Fetch the ids of movies in the library.
        """
        return list(self.library['id_movie_map'].keys())

    def fetch_user_ids(self, movie_id):
        """Fetch the user ID's from ratings of movie with id == movie_id.
        """
        mur = self.library['user_mean_centered']
        mmap_to = self.library['movie_id_map']
        mmap_from = self.library['id_movie_map']
        movie_ratings = np.isnan(mur[mmap_from[movie_id], :])
        ratings = [[i + 1, mur[mmap_from[movie_id], i]]
                   for i, r in enumerate(movie_ratings) if not r]
        return ratings

    def fetch_similarity(self, movie_1, movie_2):
        mmap = self.library['id_movie_map']
        return self.ii_matrix[mmap[movie_1]][mmap[movie_2]]

    def fetch_similar_movies(self, movieId, k=10):
        """Fetch top K similar movies according to the similarity matrix.
        """
        mim = self.library['movie_id_map']
        mmap = self.library['id_movie_map']
        sims = list(zip(map(mim.get, list(range(self.ii_matrix.shape[0]))),
                        self.ii_matrix[mmap[movieId], :]))

        # Remove the given movie ID from the list
        del sims[mmap[movieId]]

        res = sorted(sims, key=lambda e: (e[1], -e[0]), reverse=True)
        return res[:k]

    def fetch_movie_info(self, movie_id):
        """Fetch extra information about a given movie ID.
        This gives all the columns available about the movie.
        """
        movies = self.library['movies']
        return movies[movies['MovieID'] == movie_id]
