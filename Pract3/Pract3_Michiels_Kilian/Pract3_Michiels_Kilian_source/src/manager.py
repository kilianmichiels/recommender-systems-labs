#!/usr/bin/env python
""" Manager manages all the user requests. Presents the results in a nice way.
"""

# Imports
import pandas as pd
import numpy as np
from tqdm import tqdm
from time import time
import itertools as it

from librarian import Librarian
from analyst import Analyst

# Display full row without truncating
pd.set_option('display.max_colwidth', -1)

# Authorship Information
__author__ = "Kilian Michiels"
__email__ = "Kilian.Michiels@UGent.be"
__date__ = "31/03/2019"


class Manager(object):

    def __init__(self, args):
        super(Manager, self).__init__()
        self.args = args

        # [Question NR, Function, Arguments]
        self.commands = [
            [1, self.fetch_pearson, [1, 4]],
            [2, self.fetch_pearson, [1, 4]],
            [3, self.fetch_pearson, [1, 4]],
            [4, self.fetch_pearson, [1, 4]],
            [5, self.fetch_neighbourhood_size, [1, 10]],
            [6, self.fetch_neighbours, [1, 10, 20]],
            [7, self.fetch_weighted_avg_of_dev, [1, 10]],
            [8, self.fetch_user_user_cf_prediction, [1, 10]],
            [10, self.fetch_neighbourhood_size, [1, 260]],
            [11, self.fetch_neighbours, [1, 260, 20]],
            [12, self.fetch_weighted_avg_of_dev, [1, 260]],
            [13, self.fetch_user_user_cf_prediction, [1, 260]],
            [16, self.fetch_uucf_recommendations, [1]],
            [17, self.fetch_uucf_recommendations, [522]],
            [19, self.fetch_strict_pos_similarities, []],
            [20, self.fetch_cosine_similarity, [594, 596]],
            [21, self.fetch_item_item_cf_prediction, [522, 25]],
            [22, self.fetch_similar_movies, [522, 25]],
            [24, self.fetch_iicf_recommendations, [522]],
            [25, self.fetch_basket_recommendations, [[1], 10, False]],
            [26, self.fetch_basket_recommendations, [[1, 48, 239], 10, False]],
            [27, self.fetch_basket_recommendations, [[1, 48, 239], 10, True]],
            [29, self.fetch_hybrid_recommendations, [522]]
        ]

        # Set up the necessary components.
        self.librarian = Librarian(self.args)
        self.analyst = Analyst(self.librarian.library)

    ##########################################################################
    #                                   RUNNERS                              #
    ##########################################################################

    @staticmethod
    def wrapper(func, args):
        func(*args)

    def run_main_loop(self):
        """Repeat asking a question number and executing the function.
        Saves loading time.
        """
        while 1:
            q = input('Question number (1-29): ')
            try:
                q = int(q)
                self.run_question(q)
            except Exception as e:
                tqdm.write('Error: {}'.format(e))

    def run_all(self):
        """Run all the commands after each other.
        """
        for c in self.commands:
            tqdm.write('Manager - Running command for question {}...'
                       .format(c[0]))
            self.wrapper(c[1], c[2])

    def run_question(self, question):
        """Run the command associated with the given question.
        """
        self.print_full_line()
        allowed = [item[0] for item in self.commands]
        if question in allowed:
            tqdm.write('Manager - Running command for question {}...'
                       .format(question))
            c = self.commands[allowed.index(question)]
            self.wrapper(c[1], c[2])
        else:
            print('Manager - Invalid question number.')

    ##########################################################################
    #                                   METHODS                              #
    ##########################################################################

    # ---------------------------------- UUCF ------------------------------- #

    def fetch_pearson(self, user_1, user_2):
        self.print_full_line()
        tqdm.write('Manager - Calculating Pearson Correlation '
                   '(Without Significance Weighting)...')
        res_1 = self.analyst.calculate_pearson_correlation(user_1,
                                                           user_2,
                                                           sig_w=False)
        tqdm.write('Pearson Correlation between user {} and {}: {}'
                   .format(user_1, user_2, res_1))
        self.print_dotted_line()
        tqdm.write('Manager - Calculating Pearson Correlation '
                   '(With Significance Weighting)...')
        res_2 = self.analyst.calculate_pearson_correlation(user_1,
                                                           user_2,
                                                           sig_w=True)
        tqdm.write('Pearson Correlation between user {} and {}: {}'
                   .format(user_1, user_2, res_2))
        self.print_dotted_line()
        tqdm.write('Manager - Calculating Ratio...')
        res = res_1 / res_2
        tqdm.write('Ratio between answer 1 and 2: {}'.format(res))
        self.print_full_line()

    def fetch_neighbourhood_size(self, user_id, movie_id):
        self.print_full_line()
        tqdm.write('Manager - Fetching Neighbors...')
        res = self.analyst.find_neighbourhood(user_id, movie_id, 10000000)
        tqdm.write('Number of neighbors found: {}'.format(len(res)))
        tqdm.write('\nMovie Information:')
        self.fetch_movie_info_by_id([movie_id])
        self.print_full_line()

    def fetch_neighbours(self, user_id, movie_id, k):
        self.print_full_line()
        tqdm.write('Manager - Fetching Neighbors...')
        res = self.analyst.find_neighbourhood(user_id, movie_id, k)
        result = pd.DataFrame(res)
        result.columns = ['UserID', 'Ratings', 'Correlation']
        tqdm.write('Neighbors:\n{}'.format(result.to_latex()))
        tqdm.write('\nMovie Information:')
        self.fetch_movie_info_by_id([movie_id])
        self.print_full_line()

    def fetch_weighted_avg_of_dev(self, user_id, movie_id):
        self.print_full_line()
        tqdm.write('Manager - Fetching the weighted average of the deviation '
                   'from their mean rating...')
        res = self.analyst.user_user_cf(user_id, movie_id)
        tqdm.write('Value = {}'.format(res[3]))
        tqdm.write('\nMovie Information:')
        self.fetch_movie_info_by_id([movie_id])
        self.print_full_line()

    def fetch_user_user_cf_prediction(self, user_id, movie_id):
        self.print_full_line()
        tqdm.write('Manager - Fetching User-User CF Rating Prediction...')
        res = self.analyst.user_user_cf(user_id, movie_id)
        tqdm.write('Predicted Rating = {}'.format(res[2]))
        tqdm.write('\nMovie Information:')
        self.fetch_movie_info_by_id([movie_id])
        self.print_full_line()

    def fetch_uucf_recommendations(self, user_id):
        self.print_full_line()
        tqdm.write('Manager - Fetching UUCF Recommendations...')
        res = self.analyst.recommend_uucf(user_id)
        recommendations = pd.merge(res, self.librarian.library['movies'],
                                   how='inner',
                                   on='MovieID')
        result = recommendations[['MovieID', 'Title', 'Prediction']]
        tqdm.write('Recommendations:\n{}'.format(result.to_latex()))
        self.print_full_line()

    # ---------------------------------- IICF ------------------------------- #

    def fetch_strict_pos_similarities(self):
        self.print_full_line()
        tqdm.write('Manager - Fetching strict positive similarities...')
        amount = np.count_nonzero(self.analyst.ii_matrix)
        max = (self.analyst.ii_matrix.shape[0]
               * self.analyst.ii_matrix.shape[1])
        tqdm.write('Number of strict positive similarities '
                   'in the matrix: {} out of \nthe total {} values.'
                   .format(amount, max))
        self.print_full_line()

    def fetch_cosine_similarity(self, movie_1, movie_2):
        self.print_full_line()
        tqdm.write('Manager - Fetching cosine similarity between movies...')
        res = self.analyst.calculate_similarity(movie_1, movie_2)
        tqdm.write('Cosine simularity between {} and {}: {}'
                   .format(movie_1, movie_2, res[2]))
        tqdm.write('\nMovie Information:')
        self.fetch_movie_info_by_id([movie_1, movie_2])
        self.print_full_line()

    def fetch_item_item_cf_prediction(self, user_id, movie_id):
        self.print_full_line()
        tqdm.write('Manager - Fetching Item-Item CF Rating Prediction...')
        similars = self.analyst.fetch_similar_rated_movies(user_id,
                                                           movie_id,
                                                           10000000)
        sims = [s for s in similars if s[2] > 0]
        num_sims = len(sims)
        tqdm.write('Number of items with a similarity > 0: {}'
                   .format(num_sims))
        res = self.analyst.item_item_cf(user_id, movie_id)
        tqdm.write('Predicted Rating = {}'.format(res[2]))
        tqdm.write('\nMovie Information:')
        self.fetch_movie_info_by_id([movie_id])
        self.print_full_line()

    def fetch_similar_movies(self, user_id, movie_id, k=20):
        self.print_full_line()
        tqdm.write('Manager - Fetching Top K Similar Movies...')
        r = self.analyst.fetch_similar_rated_movies(user_id, movie_id, k)
        res = pd.DataFrame(r)
        res.columns = ['MovieID', 'Rating', 'Similarity']
        merged = pd.merge(res, self.librarian.library['movies'],
                          how='inner',
                          on='MovieID')
        result = merged[['MovieID', 'Title', 'Similarity']]
        tqdm.write('Similar Movies:\n{}'.format(result.to_latex()))
        tqdm.write('\nMovie Information:')
        self.fetch_movie_info_by_id([movie_id])
        self.print_full_line()

    def fetch_iicf_recommendations(self, user_id):
        self.print_full_line()
        tqdm.write('Manager - Fetching IICF Recommendations...')
        res = self.analyst.recommend_iicf(user_id)
        recommendations = pd.merge(res, self.librarian.library['movies'],
                                   how='inner',
                                   on='MovieID')
        result = recommendations[['MovieID', 'Title', 'Prediction']]
        tqdm.write('Recommendations:\n{}'.format(result.to_latex()))
        self.print_full_line()

    # --------------------------------- BASKET ------------------------------ #

    def fetch_basket_recommendations(self, basket_items, n, use_negatives):
        self.print_full_line()
        tqdm.write('Manager - Fetching Basket Recommendations...')
        res = self.analyst.recommend_basket(basket_items, n, use_negatives)
        recommendations = pd.merge(res, self.librarian.library['movies'],
                                   how='inner',
                                   on='MovieID')
        result = recommendations[['MovieID', 'Title', 'Score']]
        tqdm.write('Basket Recommendations:\n{}'.format(result.to_latex()))
        tqdm.write('\nMovie Information:')
        self.fetch_movie_info_by_id(basket_items)
        self.print_full_line()

    # --------------------------------- HYBRID ------------------------------ #

    def fetch_hybrid_recommendations(self, user_id):
        self.print_full_line()
        tqdm.write('Manager - Fetching Basket Recommendations...')
        res = self.analyst.recommend_hybrid(user_id)
        merged = pd.merge(res, self.librarian.library['movies'],
                          on='MovieID',
                          how='inner')
        result = merged[['MovieID', 'Title', 'Hybrid_Prediction']]
        tqdm.write('Hybrid Recommendations:\n{}'.format(result.to_latex()))
        self.print_full_line()

    def fetch_movie_info_by_id(self, movie_ids):
        """Fetch MovieID, Title and Genres of given movies.
        """
        self.print_full_line()
        tqdm.write('Manager - Fetching information about movie ID\'s...')
        info = pd.DataFrame(columns=['MovieID', 'Title', 'Genres'])
        for id in movie_ids:
            temp = self.analyst.fetch_movie_info(id)
            tqdm.write("\n--> ID: {}".format(id))
            tqdm.write(
                "--> Full Title: {}".format(temp[['Title']].values[0][0]))
            tqdm.write("--> All Genres: {}".format(temp[['Genres']].values[0][0])
                       .replace('|', ', '))
        self.print_full_line()

    ##########################################################################
    #                                   HELPERS                              #
    ##########################################################################

    def print_full_line(self):
        tqdm.write('-' * 82)

    def print_dotted_line(self):
        tqdm.write('- ' * 41)
