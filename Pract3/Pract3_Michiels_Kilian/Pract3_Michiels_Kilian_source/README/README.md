## Pract3 Michiels Kilian Source Code
---

Different options are possible:  
* To run the code: `python main.py`  
* To run the code with all commands from the Analysis: `python main.py --run_all`
* To run the code for a specific question from the analysis: `python main.py -q <questionNR>`

For more information: `python main.py -h`

Data needs to be placed in the data folder with the original filenames:
  * movies.csv
  * ratings.csv
  * ii_matrix.h5

---

If the item-item matrix is not present, the code will generate a new one automatically. This might take a while (up to 1 hour, depending on the number of CPU cores).
