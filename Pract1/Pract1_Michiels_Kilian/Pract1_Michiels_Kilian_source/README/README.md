## Pract1 Michiels Kilian Source Code
---

Different options are possible:  
* To run the code: `python main.py`  
* To run the code with all commands from the PDF: `python main.py --run_all`  
* To run the code for a specific question from the analysis: `python main.py -q <questionNR>`

For more information: `python main.py -h`

Data needs to be placed in the data folder with the original .dat filenames:
  * movies.dat
  * ratings.dat
  * users.dat (optional/not used)

---

Commands from the PDF are given below. This is for easy copy/paste into the application. The first command is used when running for the first time. Once the program is running, the second command can also be used.

| Question |              Command 1             |        Command 2        |
|:--------:|:----------------------------------:|:-----------------------:|
|     1    |    python main.py -i 1 1064 -m 1   |    1 1064 - 0 - 1 - 0   |
|     2    |    python main.py -i 1 1064 -m 2   |    1 1064 - 0 - 2 - 0   |
|     3    |                  /                 |            /            |
|     4    |    python main.py -i 1 2858 -m 1   |    1 2858 - 0 - 1 - 0   |
|     5    | python main.py -i 1 1064 2858 -m 6 | 1 1064 2858 - 0 - 6 - 0 |
|     6    |                  /                 |            /            |
|     7    |    python main.py -i 1 2858 -m 2   |    1 2858 - 0 - 2 - 0   |
|     8    | python main.py -i 1 1064 2858 -m 2 | 1 1064 2858 - 0 - 2 - 0 |
|     9    |      python main.py -n 10 -m 5     |      0 - 10 - 5 - 0     |
|    10    |  python main.py -i 3941 -n 5 -m 3  |     3941 - 5 - 3 - 0    |
|    11    |  python main.py -i 3941 -n 5 -m 4  |     3941 - 5 - 4 - 0    |
|    12    |                  /                 |             /           |
|    13    |                  /                 |             /           |
|    14    |   python main.py -n 10 -m 5 -s 4   |      0 - 10 - 5 - 4     |
|    15    |                  /                 |             /           |

---
