#!/usr/bin/env python
"""Argument Parser for the application.
"""

# Imports
import argparse

# Authorship Information
__author__ = "Kilian Michiels"
__email__ = "Kilian.Michiels@UGent.be"
__date__ = "21/02/2019"


def fetch_args():
    # Create parser
    parser = argparse.ArgumentParser(
        description="Recommender Systems - Practical Exercise 1: \
        A Non-Personalized Recommender.",
        formatter_class=argparse.RawTextHelpFormatter)

    # Arguments
    parser.add_argument('-i', type=int, default=[1, 1064], nargs='+',
                        help="Movie ID(s) to use in the calculations. \
                        If more than 1, the first one is X, second one is Y.")

    parser.add_argument('-n', type=int, default=5,
                        help="Number of movies with the highest association \
                        values to show.")

    parser.add_argument('-m', type=int, default=1,
                        help="Method that needs to be invoked: \
                        1 = Simple Association. 2 = Advanced Association. \
                        3 = Top-N Simple. 4 = Top-N Advanced. 5 = Frequency. \
                        6 = Fetch info.")

    parser.add_argument('-s', type=int, default=0,
                        help="Minimum number of stars to use when \
                        fetching frequency.")

    parser.add_argument('--run_all', action='store_true', default=False,
                        help="Run main with all commands from PDF.")

    parser.add_argument('-q', type=int, default=0,
                        help="Number of the question in the analysis to run.")

    args = vars(parser.parse_args())

    return args
