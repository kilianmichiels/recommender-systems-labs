#!/usr/bin/env python
"""Goal: You have to create a program that calculates for a given Movie ID the
Top-N movies with the highest association values.
"""
# Imports
import arguments
from manager import Manager

# Authorship Information
__author__ = "Kilian Michiels"
__email__ = "Kilian.Michiels@UGent.be"
__date__ = "21/02/2019"


def main():
    args = arguments.fetch_args()
    manager = Manager(args)
    if args['run_all']:  # User requests all solutions.
        manager.run_all()
    elif args['q'] > 0:  # A question number is given.
        manager.run_question()
    else:
        # Otherwise, run the main loop.
        manager.run_main_loop()


if __name__ == '__main__':
    main()
