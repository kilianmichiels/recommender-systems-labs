#!/usr/bin/env python
"""Manager manages all the user requests. Presents the results in a nice way.

Contains:
--> Method for requesting and returning the simple association.

--> Method for requesting and returning the advanced association.

--> Method for requesting and returning the Top-N MovieID's with the highest
    simple association value.

--> Method for requesting and returning the Top-N MovieID's with the highest
    advanced association value.

--> Method for requesting and returning the Top-N most rated items.

--> Method for requesting and returning information about given movie ID's.

Additionally:
--> Method to run all commands from the analysis.

--> Method to run one question from the analysis.

--> Method to run a loop, requesting new user input in the form of commands.
"""

# Imports
import pandas as pd
from time import time
import itertools as it

from librarian import Librarian
from analyst import Analyst

# Authorship Information
__author__ = "Kilian Michiels"
__email__ = "Kilian.Michiels@UGent.be"
__date__ = "21/02/2019"


class Manager(object):

    def __init__(self, args):
        super(Manager, self).__init__()
        self.args = args

        # [Question Nr, Command]
        self.commands = [
            [1,  '1 1064      - 0  - 1 - 0'],
            [2,  '1 1064      - 0  - 2 - 0'],
            [4,  '1 2858      - 0  - 1 - 0'],
            [5,  '1 1064 2858 - 0  - 6 - 0'],
            [7,  '1 2858      - 0  - 2 - 0'],
            [8,  '1 1064 2858 - 0  - 2 - 0'],
            [9,  '0           - 10 - 5 - 0'],
            [10, '3941        - 5  - 3 - 0'],
            [11, '3941        - 5  - 4 - 0'],
            [14, '0           - 10 - 5 - 4']
        ]

        # Set up the necessary components.
        self.librarian = Librarian(self.args)
        self.analyst = Analyst(self.librarian.library)

    ##########################################################################
    #                                   RUNNERS                              #
    ##########################################################################

    def run_all(self):
        """Run all the commands after each other.
        """
        self.print_full_line()
        for command in self.commands:
            print('Manager - Running command for question {}...'
                  .format(command[0]))
            self.set_input(from_user=False, command=command[1])
            self.check_requested_method()

    def run_question(self):
        """Run the command associated with the given question.
        """
        self.print_full_line()
        question = int(self.args['q'])
        allowed = [1, 2, 4, 5, 7, 8, 9, 10, 11, 14]
        if question in allowed:
            print('Manager - Running command for question {}...'
                  .format(question))
            self.set_input(from_user=False, command=self.commands[allowed.index(question)][1])
            self.check_requested_method()
        else:
            print('Manager - Invalid question number.')

    def run_main_loop(self):
        """Main loop to make sure the dataset does not need to be
        loaded for every request.

        Processes the first arguments from argparser and asks for
        new parameters when done.

        """
        print('\nManager - Running main loop... '
              'To quit program, press ctrl-c.')
        self.print_full_line()
        self.give_explanation()
        self.check_requested_method()
        while(True):
            try:
                self.set_input()
                self.check_requested_method()
            except Exception as e:
                print('Manager - Invalid input.')

    ##########################################################################
    #                                   METHODS                              #
    ##########################################################################

    def fetch_simple_association(self):
        """Fetch the simple assocation for the given arguments.

        """
        self.print_full_line()
        print('Manager - Calculating simple association...')
        for ids in it.combinations(self.args['i'], 2):
            print('--> Checking combination (X, Y): {}'.format(ids))
            for id in range(1, len(ids)):
                simple_a = self.analyst.calculate_simple_association(
                    ids[0], ids[id])
                print('--> Simple Association: {}'.format(simple_a[0]))
                self.print_dotted_line()
                print('--> X = {}\n--> Y = {}\n--> X and Y = {}'.format(
                    simple_a[2], simple_a[3], simple_a[4]))
                self.print_full_line()

    def fetch_advanced_association(self):
        """Fetch the advanced assocation for the given arguments.

        """
        self.print_full_line()
        print('Manager - Calculating advanced association...')
        for ids in it.combinations(self.args['i'], 2):
            print('--> Checking combination (X, Y): {}'.format(ids))
            for id in range(1, len(ids)):
                advanced_a = self.analyst.calculate_advanced_association(
                    ids[0], ids[id])
                print('--> Advanced Association: {}'.format(advanced_a[0]))
                self.print_dotted_line()
                print('--> X = {}\n--> Y = {}\n--> X and Y = {}'.format(
                    advanced_a[2], advanced_a[3], advanced_a[4]))
                print('--> Not X = {}\n--> Not X and Y = {}'.format(
                    advanced_a[5], advanced_a[6]))
                self.print_full_line()

    def fetch_top_n_simple(self):
        """Fetch the top N movies with the highest simple association
        for a given movie ID.

        """
        self.print_full_line()
        print('Manager - Calculating Top-N simple association...')
        movie_ids, associations = self.analyst.fetch_top_n_simple(
            self.args['i'][0], self.args['n'])
        info = pd.DataFrame(columns=['MovieID', 'Title', 'Association Value'])
        for id, association in zip(movie_ids, associations):
            temp = self.analyst.fetch_movie_info(id)[['MovieID', 'Title']]
            temp['Association Value'] = association
            info = info.append(temp)
        print(info.reset_index(drop=True))
        self.print_full_line()

    def fetch_top_n_advanced(self):
        """Fetch the top N movies with the highest advanced association
        for a given movie ID.

        """
        self.print_full_line()
        print('Manager - Calculating Top-N advanced association...')
        movie_ids, associations = self.analyst.fetch_top_n_advanced(
            self.args['i'][0], self.args['n'])
        info = pd.DataFrame(columns=['MovieID', 'Title', 'Association Value'])
        for id, association in zip(movie_ids, associations):
            temp = self.analyst.fetch_movie_info(id)[['MovieID', 'Title']]
            temp['Association Value'] = association
            info = info.append(temp)
        print(info.reset_index(drop=True))
        self.print_full_line()

    def fetch_highest_frequency(self):
        """Fetch the most rated movies.

        """
        self.print_full_line()
        print('Manager - Calculating most N rated items.')
        freq = self.analyst.fetch_highest_frequency(
            self.args['n'], self.args['s'])
        info = pd.DataFrame(columns=['MovieID', 'Title', 'Genres', '#Ratings'])
        for i, (id, count) in enumerate(zip(freq['MovieID'], freq['Count'])):
            temp = self.analyst.fetch_movie_info(
                id)[['MovieID', 'Title', 'Genres']]
            temp['#Ratings'] = count
            info = info.append(temp)
        print(info.reset_index(drop=True))
        self.print_full_line()

    def fetch_movie_info_by_id(self):
        """Fetch MovieID, Title and Genres of given movies.

        """
        self.print_full_line()
        print('Manager - Fetching information about movie ID\'s...')
        info = pd.DataFrame(columns=['MovieID', 'Title', 'Genres'])
        for id in self.args['i']:
            temp = self.analyst.fetch_movie_info(id)
            print("\n--> ID: {}".format(id))
            print("--> Full Title: {}".format(temp[['Title']].values[0][0]))
            print("--> All Genres: {}".format(temp[['Genres']].values[0][0])
                  .replace('|', ', '))
        self.print_full_line()

    ##########################################################################
    #                                   HELPERS                              #
    ##########################################################################

    def give_explanation(self):
        print('EXPLANATION: Each round, input is requested.\n'
              'You have to input all fields but only the relevant '
              'ones\nwill be used. Example: We want to calculate '
              'the simple\nassociation between Movie 1 and 10.\n'
              'Then the input should be: 1 10 - X - 1 - X .\n'
              'The X can be any integer value.')

    def set_input(self, from_user=True, command=None):
        """Set the input arguments to the latest command.

        This command can be either given from the user of from
        the program itself.

        Parameters
        ----------
        from_user : bool
            True if we accept user input.
        command : str
            The command in case no user input is requested.

        """

        args = None
        if from_user:
            user_provided_input = input(
                '\nPlease provide: Movie ID(s) - N - Method Number - '
                'Min. Stars (Example: 1 - 5 - 3 - 0): ')
            args = user_provided_input.split('-')
        else:
            args = command.split('-')
        self.args['i'] = list(map(int, args[0].strip().split(' ')))
        self.args['n'] = int(args[1].strip())
        self.args['m'] = int(args[2].strip())
        self.args['s'] = int(args[3].strip())

    def check_requested_method(self):
        """Check whether the requested method number is valid and the
        necessary arguments passed are correctly.
        If the requested method is valid and the arguments are correct,
        this method runs.

        """
        if self.args['m'] > 6 or self.args['m'] < 1:
            print('Manager - Method {} not available.'.format(self.args['m']))
        else:
            if self.args['m'] == 1 and self.check_id_list_length():
                self.fetch_simple_association()
            elif self.args['m'] == 2 and self.check_id_list_length():
                self.fetch_advanced_association()
            elif self.args['m'] == 3:
                self.fetch_top_n_simple()
            elif self.args['m'] == 4:
                self.fetch_top_n_advanced()
            elif self.args['m'] == 5:
                self.fetch_highest_frequency()
            elif self.args['m'] == 6:
                self.fetch_movie_info_by_id()

    def check_id_list_length(self):
        """Check whether we have enough ID's in args to do calculations.

        """
        if len(self.args['i']) >= 2:
            return True
        else:
            print('\nError: Expecting 2 or more Movie ID\'s but got less.')
            return False

    def print_full_line(self):
        print('-' * 62)

    def print_dotted_line(self):
        print('- ' * 31)
