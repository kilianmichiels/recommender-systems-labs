#!/usr/bin/env python
"""Librarian handles all data read/write operations.

Fetch data, convert data into the correct format, store data,...
"""

# Imports
import pandas as pd
from tqdm import tqdm

# Authorship Information
__author__ = "Kilian Michiels"
__email__ = "Kilian.Michiels@UGent.be"
__date__ = "21/02/2019"


class Librarian(object):

    def __init__(self, args=None):
        super(Librarian, self).__init__()
        self.args = args

        self.library = dict()

        # File locations:
        self.ratings_location = "../data/ratings.dat"
        self.movies_location = "../data/movies.dat"

        # Basic set up
        self.read_data()

    def read_data(self):
        print('Librarian - Reading and processing .dat files...')
        with open(self.ratings_location, 'r') as f:
            self.library['ratings'] = pd.DataFrame(
                l.strip().split('::') for l in tqdm(f))
        self.library['ratings'].columns = [
            'UserID', 'MovieID', 'Rating', 'Timestamp']

        # Convert string to numeric
        for col in self.library['ratings'].columns:
            self.library['ratings'][col] = pd.to_numeric(
                self.library['ratings'][col])

        with open(self.movies_location, 'rb') as f:
            self.library['movies'] = pd.DataFrame(
                l.decode('latin-1').strip().split('::') for l in tqdm(f))
        self.library['movies'].columns = ['MovieID', 'Title', 'Genres']

        # Convert string to numeric
        self.library['movies']['MovieID'] = pd.to_numeric(
            self.library['movies']['MovieID'])
