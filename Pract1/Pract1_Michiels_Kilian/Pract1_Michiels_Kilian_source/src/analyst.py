#!/usr/bin/env python
"""Analyst performs calculations on the data.

Contains:
--> A method to calculate the simple association value of 2 movies.

--> A method to calculate the advanced association value of 2 movies.

--> A method that provides for a given MovieID, the Top-N MovieID's
    with the highest simple association value. In case of a tie, the
    movie with the higher ID is ranked before the movie with the lower ID.

--> A method that provides for a given MovieID, the Top-N MovieIDs with
    the highest advanced association value. In case of a tie, the movie
    with the higher ID is ranked before the movie with the lower ID.

--> A method to select the Top-N MovieID's with the highest frequency.
    This means, a list of the most rated items.
"""

# Imports
import pandas as pd
from tqdm import tqdm
from multiprocessing import Pool, cpu_count
from time import time

# Authorship Information
__author__ = "Kilian Michiels"
__email__ = "Kilian.Michiels@UGent.be"
__date__ = "21/02/2019"


class Analyst(object):

    def __init__(self, library):
        super(Analyst, self).__init__()
        self.library = library

    def calculate_simple_association(self, movie_id_1, movie_id_2):
        """A method to calculate the simple association value of 2 movies.

        Parameters
        ----------
        movie_id_1 : int
            The ID of the first movie. This is used as the X in the formula.
        movie_id_2 : int
            The ID of the second movie. This is used as the Y in the formula.

        Returns
        -------
        float
            Simple association between movie 1 and 2.

        """
        # Select the users who rated the movies
        users_movie_1 = self.fetch_user_ids(movie_id_1)
        users_movie_2 = self.fetch_user_ids(movie_id_2)

        # Select the nr of users who rated the movies
        X = len(users_movie_1)
        Y = len(users_movie_2)

        # Now we simply cross reference:
        # --> Count number of True
        X_and_Y = users_movie_2.isin(users_movie_1).sum()

        result = 1.0 * X_and_Y / X
        return [result, movie_id_2, X, Y, X_and_Y]

    def calculate_advanced_association(self, movie_id_1, movie_id_2):
        """A method to calculate the advanced association value of 2 movies.

        Parameters
        ----------
        movie_id_1 : int
            The ID of the first movie. This is used as the X in the formula.
        movie_id_2 : int
            The ID of the second movie. This is used as the Y in the formula.

        Returns
        -------
        float
            Advanced association between movie 1 and 2.

        """
        # Select the users who rated the movies
        users_movie_1 = self.fetch_user_ids(movie_id_1)
        users_movie_2 = self.fetch_user_ids(movie_id_2)

        # Select the nr of users who rated the movies
        X = len(users_movie_1)
        Y = len(users_movie_2)

        # Select the nr of users who did not rate movie X
        not_X = self.fetch_nr_unique_users() - X

        # Now we simply cross reference:
        count = users_movie_2.isin(users_movie_1).value_counts()
        X_and_Y = count[1] if True in count.index else 0  # Nr of True
        not_X_and_Y = count[0] if False in count.index else 0  # Nr of False

        # Add very small number to not have division by zero.
        result = (1.0 * X_and_Y / (X + 1e-32)) / ((not_X_and_Y + 1e-32) / (not_X + 1e-32))
        return [result, movie_id_2, X, Y, X_and_Y, not_X_and_Y, not_X]

    def fetch_top_n_simple(self, movie_id, n):
        """Calculate the Top-N MovieID's with the highest simple
            association value for a given movie ID. In case of a tie, the movie
            with the higherID is ranked before the movie with the lowerID.

        Parameters
        ----------
        movie_id : int
            The ID of the movie.
        n : int
            Amount of movies to return from the top.

        Returns
        -------
        list
            List of Top-N MovieID's with the highest simple association value.
            Ranked from high to low.
        """
        movie_ids = self.fetch_movie_ids()

        # Calculate all the assocations
        num_cpus = cpu_count() - 1
        print('Analyst - Working with {} processes.'.format(num_cpus))
        pool = Pool(processes=num_cpus)
        func_args = [(movie_id, id) for id in movie_ids]
        # Remove the association of (X=movie_id, Y=movie_id).
        del func_args[self.fetch_movie_index(movie_id)]

        start = time()
        results = pool.starmap_async(
            self.calculate_simple_association, func_args)
        pool.close()
        pool.join()
        print('Analyst - Parallel Processing Time: {:.4f} seconds.'
              .format(time() - start))

        r = results.get()
        # Sort on association and next on ID (highest first)
        r = sorted(r, key=lambda e: (e[0], e[1]), reverse=True)

        # Split id's and association results
        associations = []
        movie_ids = []
        for res in r:
            associations.append(res[0])
            movie_ids.append(res[1])

        # Find the top N
        return movie_ids[:n], associations[:n]

    def fetch_top_n_advanced(self, movie_id, n):
        """Calculate the Top-N MovieID's with the highest advanced
            association value for a given movie ID. In case of a tie, the movie
            with the higherID is ranked before the movie with the lowerID.

        Parameters
        ----------
        movie_id : int
            The ID of the movie.
        n : int
            Amount of movies to return from the top.

        Returns
        -------
        list
            List of Top-N MovieID's with the highest simple association value.
            Ranked from high to low.
        """
        movie_ids = self.fetch_movie_ids()

        # Calculate all the assocations
        num_cpus = cpu_count() - 1
        print('Analyst - Working with {} processes.'.format(num_cpus))
        pool = Pool(processes=num_cpus)
        func_args = [(movie_id, id) for id in movie_ids]
        # Remove the association of (X=movie_id, Y=movie_id).
        del func_args[self.fetch_movie_index(movie_id)]

        start = time()
        results = pool.starmap_async(
            self.calculate_advanced_association, func_args)
        pool.close()
        pool.join()
        print('Analyst - Parallel Processing Time: {:.4f} seconds.'
              .format(time() - start))

        r = results.get()
        # Sort on association and next on ID (highest first)
        r = sorted(r, key=lambda e: (e[0], e[1]), reverse=True)

        # Split id's and association results
        associations = []
        movie_ids = []
        for res in r:
            associations.append(res[0])
            movie_ids.append(res[1])

        # Find the top N
        return movie_ids[:n], associations[:n]

    def fetch_highest_frequency(self, n, min_stars):
        """A method to select the Top-N MovieIDs with the highest frequency.
        This means, a list of the most rated items.

        Parameters
        ----------
        n : int
            Amount of movies to return from the top.

        min_stars : int
            Minimum number of stars the movies need to have.

        Returns
        -------
        list
            List of N most rated items.
        """
        num_ratings = self.fetch_num_ratings(min_stars)
        return num_ratings.nlargest(n, columns=['Count'])

    ##########################################################################
    #                                   HELPERS                              #
    ##########################################################################

    def fetch_nr_unique_users(self):
        """Fetch the number of unique users.
        """
        ratings = self.library['ratings']
        num_users = len(ratings['UserID'].unique())
        return num_users

    def fetch_movie_ids(self):
        """Fetch the ids of movies in the library.
        """
        return self.library['movies']['MovieID'].values

    def fetch_movie_index(self, movie_id):
        """Fetch the index of the movie in the library.
        """
        movies = self.library['movies']
        return movies[movies['MovieID'] == movie_id].index[0]

    def fetch_user_ids(self, movie_id):
        """Fetch the user ID's from ratings of movie with id == movie_id.
        """
        ratings = self.library['ratings']
        return ratings[ratings['MovieID'] == movie_id]['UserID']

    def fetch_num_ratings(self, min_stars):
        """Fetch the number of ratings given to a movie.
        Ratings must be >= min_stars.
        """
        ratings = self.library['ratings']
        return (ratings[ratings['Rating'] >= min_stars]
                .groupby(['MovieID']).size().reset_index(name='Count'))

    def fetch_movie_info(self, movie_id):
        movies = self.library['movies']
        return movies[movies['MovieID'] == movie_id]
