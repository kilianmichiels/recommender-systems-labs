## Pract2 Michiels Kilian Source Code
---

Different options are possible:  
* To run the code: `python main.py`  
* To run the code with all commands from the Analysis: `python main.py --run_all`
* To run the code for a specific question from the analysis: `python main.py -q <questionNR>`

For more information: `python main.py -h`

Data needs to be placed in the data folder with the original .csv filenames:
  * movies.csv
  * ratings.csv
  * users.csv (optional/not used)

---

Due to the Sugestio implementation, certain waiting/confirmation statements are implemented in the source code to take calculation times into account. When running all commands, the code will request user input to continue when calculations are done.
