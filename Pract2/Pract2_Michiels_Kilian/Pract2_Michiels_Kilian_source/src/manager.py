#!/usr/bin/env python
"""Manager manages all the user requests. Presents the results in a nice way.
"""

# Imports
import time

from analyst import Analyst
from translator import Translator
from librarian import Librarian
import pandas as pd
pd.set_option('display.max_colwidth', -1)  # Display full row without truncating

# Authorship Information
__author__ = "Kilian Michiels"
__email__ = "Kilian.Michiels@UGent.be"
__date__ = "15/03/2019"


class Manager(object):
    """
    Manager manages the program. It is able to answer 1 question, run all
    commands from the PDF or run a loop requesting new input at each round.
    """
    def __init__(self, args):
        super(Manager, self).__init__()
        self.args = args

        # [Question Nr, Command (M - UID - MID - N - S)]
        self.commands = [
            [1,  '2 -   289     -   1125    -   0   -   0'],
            [0,  'Select collaborative filtering with explicit rating data in '
                 'the Settings tab. Schedule a job to update personalized '
                 'recommendations. Only continue when the job has finished.'],
            [3,  '2 -   249     -   0       -   0   -   0'],
            [4,  '1 -   249     -   0       -   5   -   0'],
            [5,  '2 -   35      -   0       -   0   -   0'],
            [6,  '1 -   35      -   0       -   5   -   0'],
            [0,  'Select content based with explicit rating data in the '
                 'Settings tab. Schedule a job to update personalized '
                 'recommendations. Only continue when the job has finished.'],
            [8,  '1 -   249     -   0       -   5   -   0'],
            [9,  '1 -   35      -   0       -   5   -   0'],
            [0,  'Adding new user 1000 and uploading new ratings.'],
            [10, '3 -   1000    -   0       -   0   -   0'],
            [0,  'Select collaborative filtering with explicit rating data in '
                 'the Settings tab. Schedule a job to update personalized '
                 'recommendations. Only continue when the job has finished.'],
            [12, '1 -   1000    -   0       -   10  -   0'],
            [0,  'Select content based with explicit rating data in the '
                 'Settings tab. Schedule a job to update personalized '
                 'recommendations. Only continue when the job has finished.'],
            [14, '1 -   1000    -   0       -   10  -   0'],
            [15, '4 -   1000    -   6587    -   0   -   1.0'],
            [0,  'Uploading new rating might take some time to register. Wait '
                 'some time before continuing.'],
            [15, '5 -   0       -   6587    -   0   -   0'],
            [0,  'Schedule a job to update personalized recommendations. '
                 'Only continue when the job has finished.'],
            [17, '1 -   1000    -   0       -   10   -   0'],
            [0,  'Deleting all the ratings of user 1000.'],
            [18, '6 -   1000    -   0       -   0   -   0']
        ]

        # Set up the necessary components.
        self.librarian = Librarian(self.args['v'])
        self.translator = Translator(self.librarian)
        self.analyst = Analyst()

        if self.args['upload_data']:
            self.librarian.upload()

    ##########################################################################
    #                                   RUNNERS                              #
    ##########################################################################

    def run_all(self):
        """Run all the commands after each other.
        """
        self.print_full_line()
        question_nrs = [item[0] for item in self.commands]
        q = int(self.args['start_with'])
        for index in range(question_nrs.index(q), len(self.commands)):
            if self.commands[index][0] != 0:
                print('Manager - Running command for question {}...'
                      .format(self.commands[index][0]))
                self.set_input(from_user=False, command=self.commands[index][1])
                self.check_requested_method()
            else:
                if not self.warn_user(self.commands[index][1]):
                    index += 1
                    print('Not performing this step...')
                else:
                    print('Manager - Continuing...')

    def run_question(self):
        """Run the command associated with the given question.
        """
        self.print_full_line()
        question = int(self.args['q'])
        allowed = [item[0] for item in self.commands]
        if question in allowed:
            print('Manager - Running command for question {}...'
                  .format(question))
            self.set_input(from_user=False,
                           command=self.commands[allowed.index(question)][1])
            self.check_requested_method()
        else:
            print('Manager - Invalid question number.')

    def run_main_loop(self):
        """Processes the first arguments from argparser and asks for
        new parameters when done.
        """
        print('\nManager - Running main loop... '
              'To quit program, press ctrl-c.')
        self.print_full_line()
        self.give_explanation()
        self.check_requested_method()
        while True:
            try:
                self.set_input()
                self.check_requested_method()
            except Exception:
                print('Manager - Invalid input.')

    ##########################################################################
    #                                   METHODS                              #
    ##########################################################################

    def fetch_top_n(self):
        """Fetch the simple association for the given arguments.
        """
        self.print_full_line()
        print('Manager - Fetching top N recommendations...')
        result = self.librarian.fetch_top_n(self.args['user_id'],
                                            self.args['n'])
        if result is None:
            self.print_error()
        else:
            result = self.translator.parse_user_recommendations(result)
            print(result.to_string())
        self.print_full_line()

    def fetch_user_ratings(self):
        """
        Fetch the ratings for a given user ID. Present them in a readable way.
        """
        self.print_full_line()
        print('Manager - Fetching user ratings...')
        result = self.librarian.fetch_user_ratings(self.args['user_id'],
                                                   self.args['movie_id'])
        if result is None:
            self.print_error()
        else:
            result = self.translator.parse_user_ratings(result)
            print(result)
            self.analyst.analyse_data(result)

        self.print_full_line()

    def add_user_with_ratings(self):
        """
        Create and add new user to the Sugestio database.
        Next, also upload the user's ratings to Sugestio.
        """
        self.print_full_line()
        print('Manager - Adding user {} to Sugestio database...'
              .format(self.args['user_id']))
        self.librarian.add_user(self.args['user_id'])

        ids = [1590, 1196, 4878, 589, 480]
        scores = [4.0, 4.5, 4.0, 4.5, 4.5]
        timestamps = [1476640644, 1476640644, 1476640644,
                      1476640644, 1476640644]

        for movie_id, score, timestamp in zip(ids, scores, timestamps):
            self.librarian.add_rating(self.args['user_id'], movie_id,
                                      score, timestamp)

        print('Manager - User and ratings added...')
        print('Manager - It takes some time before the user is registered on '
              'Sugestio. Waiting 5 minutes before requesting the rating '
              'history of the newly added user... If you do not want to wait, '
              'simply exit the program with ctrl-C.')
        self.countdown(60 * 5)
        self.fetch_user_ratings()
        self.print_full_line()

    def add_rating(self):
        """
        Add one rating to the Sugestio database with the given User ID, Movie ID
        and Score.
        """
        self.print_full_line()
        print('Manager - Adding rating to user...')
        self.librarian.add_rating(self.args['user_id'], self.args['movie_id'],
                                  self.args['s'], 1476640644)
        self.print_full_line()

    def fetch_movie_information(self):
        """
        Fetch information about the given Movie ID.
        """
        self.print_full_line()
        print('Manager - Fetching movie information...')
        result = self.librarian.fetch_movie(self.args['movie_id'])
        if result is None:
            self.print_error()
        else:
            result = self.translator.parse_movie(result)
            print(result)

    def full_delete_user(self):
        """
        Fully delete a given user (ID and ratings) from the Sugestio database.
        """
        self.print_full_line()
        print('Manager - Deleting user...')
        self.librarian.delete_user_ratings(self.args['user_id'])
        self.librarian.delete_user(self.args['user_id'])

    ##########################################################################
    #                                   HELPERS                              #
    ##########################################################################

    @staticmethod
    def give_explanation():
        """
        Explain what the user needs to do.
        """
        print('EXPLANATION: Each round, input is requested.\n'
              'You have to input all fields but only the relevant '
              'ones\nwill be used. Example: We want to fetch '
              'the top 5\nrecommendations for user 1.\n'
              'Then the input should be: 2 - 1 - X - 5 - X.\n'
              'The X can be any integer value.')

    def set_input(self, from_user=True, command=None):
        """Set the input arguments to the latest command.

        This command can be either given from the user of from
        the program itself.

        @param from_user: bool. True if we accept user input.
        @param command: str. The command in case no user input is requested.
        """

        if from_user:
            user_provided_input = input(
                '\nPlease provide: Method - UserID - MovieID - Limit - Score '
                '(Example: 2 - 1 - 0 - 5 - 0): ')
            args = user_provided_input.split('-')
        else:
            args = command.split('-')
        self.args['m'] = int(args[0].strip())
        self.args['user_id'] = int(args[1].strip())
        self.args['movie_id'] = int(args[2].strip())
        self.args['n'] = int(args[3].strip())
        self.args['s'] = float(args[4].strip())

    def check_requested_method(self):
        """Check whether the requested method number is valid and the
        necessary arguments passed are correctly.
        If the requested method is valid and the arguments are correct,
        this method runs.

        """
        if self.args['m'] == 1:
            self.fetch_top_n()
        elif self.args['m'] == 2:
            self.fetch_user_ratings()
        elif self.args['m'] == 3:
            self.add_user_with_ratings()
        elif self.args['m'] == 4:
            self.add_rating()
        elif self.args['m'] == 5:
            self.fetch_movie_information()
        elif self.args['m'] == 6:
            self.full_delete_user()

    @staticmethod
    def print_error():
        """
        Print the Manager error message.
        """
        print('Manager - Could not complete request.')

    @staticmethod
    def print_full_line():
        """
        Print a full line.
        """
        print('-' * 62)

    @staticmethod
    def print_dotted_line():
        """
        Print a dotted line.
        """
        print('- ' * 31)

    @staticmethod
    def countdown(t):
        """
        Show a countdown.
        From https://stackoverflow.com/questions/25189554/countdown-clock-0105
        @param t: Time in seconds.
        """
        while t:
            minutes, secs = divmod(t, 60)
            time_format = '{:02d}:{:02d}'.format(minutes, secs)
            print(time_format, end='\r')
            time.sleep(1)
            t -= 1
        print('')

    @staticmethod
    def warn_user(info):
        """
        Warn the user about something and ask confirmation.
        @return: True if user agrees. Else, False.
        """
        print('Manager - ' + info)
        response = input('Do you want to continue? '
                         '(y/[n]) ')
        if response is 'y' or response is 'Y':
            return True
        else:
            return False
