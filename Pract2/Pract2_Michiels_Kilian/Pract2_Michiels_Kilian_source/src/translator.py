#!/usr/bin/env python
"""Translator converts API requests into python objects.
"""
# Imports
import pandas as pd

# Authorship Information
__author__ = "Kilian Michiels"
__email__ = "Kilian.Michiels@UGent.be"
__date__ = "16/03/2019"


class Translator(object):
    """
    Translator parses API responses to a DataFrame which can be processed
    more easily.
    """
    def __init__(self, librarian):
        super(Translator, self).__init__()
        self.librarian = librarian

    def parse_user_ratings(self, ratings):
        """
        Parse the API response for user ratings to a pandas dataframe.
        @param ratings: API response with ratings and information.
        @return: DataFrame with ratings and information.
        """
        df = pd.DataFrame(columns=['UserID', 'MovieID', 'Title', 'Rating',
                                   'Genres', 'Date', 'Time'])
        for i, r in enumerate(ratings):
            movie_id = r.itemid
            movie = self.librarian.fetch_movie(movie_id)
            user_id = int(r.userid)
            title = movie.title
            rating = float(self.parse_rating(r.detail))
            genres = movie.category
            date = self.parse_date(r.date)
            df.loc[i] = [user_id, movie_id, title, rating, genres,
                         date[0], date[1]]

        return df.sort_values(['Rating'], ascending=False)

    def parse_user_recommendations(self, recommendations):
        """
        Parse the API response for user recommendations to a pandas dataframe.
        @param recommendations: API response with information.
        @return: DataFrame with parsed information.
        """
        df = pd.DataFrame(columns=['MovieID', 'Title', 'Genres', 'Score',
                                   'Certainty'])
        for i, r in enumerate(recommendations):
            movie_id = int(r.item.id)
            title = r.item.title
            genres = r.item.category
            score = float(r.score)
            certainty = float(r.certainty)
            df.loc[i] = [movie_id, title, genres, score, certainty]

        return df.sort_values(['Score'], ascending=False)

    def parse_movie(self, movie_str):
        """
        Parse the API response for the movie information to a pandas dataframe.
        @param movie_str: API response with information.
        @return: DataFrame with parsed information.
        """
        df = pd.DataFrame(columns=['MovieID', 'Title', 'Genres'])

        movie_id = movie_str.id
        title = movie_str.title
        genres = movie_str.category
        df.loc[0] = [movie_id, title, genres]

        return df

    def parse_rating(self, rating_str):
        """
        Parse the API string format STAR:MAX:MIN:SCORE to the score.
        @param rating_str: String with format STAR:MAX:MIN:SCORE.
        @return: Score
        """
        return rating_str.split(':')[3]

    def parse_date(self, date_str):
        """
        Parse the API string format yyyy-mm-ddThh-mm-ss to a readable
        date and time.
        @param date_str: String with format yyyy-mm-ddThh-mm-ss.
        @return: List of strings, first the day, next the time.
        """
        date = date_str.split('T')
        y, m, d = date[0].split('-')
        date[0] = '{}-{}-{}'.format(d, m, y)
        return date
