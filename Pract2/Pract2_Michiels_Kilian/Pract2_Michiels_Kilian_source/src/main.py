#!/usr/bin/env python
"""Goal: The goal is to interact with a recommendation service.
"""
# Imports
from arguments import *
from manager import Manager

# Authorship Information
__author__ = "Kilian Michiels"
__email__ = "Kilian.Michiels@UGent.be"
__date__ = "15/03/2019"


def main():
    """
    This is where our journey begins... A manager handles the interaction with
    the user and displays everything in a very readable manner. To function
    properly, this manager needs a Librarian who talks to the server, an
    Analyst who gives some statistics about the data and a Translator who
    converts the API responses into python manageable objects.

    One can choose between 3 options:
    1) Run the manager in an argument based manner: Provide input and the
                                                    manager responds.
    2) Ask for a specific question: Provide a question number and the manager
                                    only answers that question.
    3) Run all questions: Ask the manager to run the whole program and respond
                            to all questions.
    """
    args = fetch_args()
    manager = Manager(args)
    if args['run_all']:
        manager.run_all()
    elif args['q'] > 0:
        manager.run_question()
    else:
        # Otherwise, run the main loop.
        manager.run_main_loop()


if __name__ == '__main__':
    main()
