#!/usr/bin/env python
"""Librarian handles all data read/write operations.

Fetch data, convert data into the correct format, store data,...
"""

# Imports
import pandas as pd
from tqdm import tqdm
import sugestio

# Authorship Information
__author__ = "Kilian Michiels"
__email__ = "Kilian.Michiels@UGent.be"
__date__ = "15/03/2019"


class Librarian(object):
    """
    Librarian handles communication with Sugestio and
    requests/uploads/deletes data.
    """
    def __init__(self, verbose=False):
        super(Librarian, self).__init__()
        self.verbose = verbose

        self.library = dict()

        # Set up Sugestio client side
        self.ACCOUNT = 's2019michie'
        self.SECRET = 'Zzp9oe86B0y7EX9c'
        self.client = sugestio.Client(self.ACCOUNT, self.SECRET, self.verbose)

        # File locations:
        self.ratings_location = "../data/ratings.csv"
        self.movies_location = "../data/movies.csv"

    def read_data(self):
        """
        Read CSV files and convert them to a DataFrame.
        """
        print('Librarian - Reading and processing .csv files...')
        self.library['ratings'] = pd.read_csv(self.ratings_location,
                                              dtype={'userId': int,
                                                     'movieId': int,
                                                     'rating': float,
                                                     'timestamp': int})
        self.library['ratings'].columns = ['UserID', 'MovieID', 'Rating',
                                           'Timestamp']

        # Convert string to numeric
        for col in self.library['ratings'].columns:
            self.library['ratings'][col] = pd.to_numeric(
                self.library['ratings'][col])

        self.library['movies'] = pd.read_csv(self.movies_location,
                                             dtype={'movieId': int,
                                                    'title': str,
                                                    'genres': str})
        self.library['movies'].columns = ['MovieID', 'Title', 'Genres']

        # Convert string to numeric
        self.library['movies']['MovieID'] = pd.to_numeric(
            self.library['movies']['MovieID'])

    def upload(self):
        """
        1. Read CSV files
        2. Convert genres to arrays
        3. Inform the user about the data
        4. Convert data to Sugestio objects
        5. Upload data to Sugestio
        """
        self.read_data()  # 1
        self.convert_genres()  # 2
        self.inform()  # 3
        self.data_2_sugestio()  # 4
        self.upload_2_sugestio()  # 5

    def convert_genres(self):
        """
        Process and convert the genres from the CSV files.
        """
        print('Librarian - Converting the movie genres...')

        def split_genre(row):
            """
            Split genres from Genre1 | Genre2 to an array of genres.
            @param row: One row in the DataFrame
            @return: Array of genres
            """
            if "(no genres listed)" in row.Genres:
                row.Genres = None
            else:
                row.Genres = row.Genres.split('|')
            return row

        self.library['movies'] = self.library['movies'].apply(split_genre,
                                                              axis=1)

    def inform(self):
        """
        Inform the user about the processed information found in the CSV files.
        """
        print('Librarian - Data processed.')
        print('Librarian - Found {} ratings.'
              .format(len(self.library['ratings'])))
        print('Librarian - Found {} movies.'
              .format(len(self.library['movies'])))

    def data_2_sugestio(self):
        """
        Convert library to Sugestio objects, ready to upload.
        """
        self.library['items'] = []
        self.library['users'] = []
        self.library['consumptions'] = []

        # First create the items (in this case the movies)
        pbar = tqdm(desc='Creating Items', total=len(self.library['movies']))
        for index, row in self.library['movies'].iterrows():
            item = sugestio.Item(id=str(row.MovieID))
            item.category = row.Genres
            item.title = row.Title
            self.library['items'].append(item)
            pbar.update(1)
        pbar.close()

        # Next, create the users
        users = self.library['ratings']['UserID'].unique()
        users = list(map(int, users))  # Remove .0
        users = list(map(str, users))

        pbar = tqdm(desc='Creating Users', total=len(users))
        for userID in users:
            user = sugestio.User(id=userID)
            self.library['users'].append(user)
            pbar.update(1)
        pbar.close()

        # Finally create the consumptions (in this case the ratings)
        pbar = tqdm(desc='Creating Consumptions',
                    total=len(self.library['ratings']))
        for index, row in self.library['ratings'].iterrows():
            consumption = sugestio.Consumption(userid=str(int(row.UserID)),
                                               itemid=str(int(row.MovieID)))
            consumption.type = "RATING"
            consumption.detail = "STAR:5:0.5:{}".format(row.Rating)
            consumption.date = row.Timestamp
            self.library['consumptions'].append(consumption)
            pbar.update(1)
        pbar.close()

    def upload_2_sugestio(self):
        """Upload the data to Sugestio.
        """
        print('Librarian - Uploading movies...')
        self.client.add_items(self.library['items'])
        print('Librarian - Uploading users...')
        self.client.add_users(self.library['users'])
        print('Librarian - Uploading ratings...')
        self.client.add_consumptions(self.library['consumptions'])
        print('Librarian - Data uploaded to Sugestio.')

    ##########################################################################
    #                               DATA RETRIEVAL                           #
    ##########################################################################

    def fetch_top_n(self, user_id, n):
        """
        Fetch the top N recommendations from the Sugestio system.
        @param user_id: User ID
        @param n: Max number of recommendations
        @return: API response if success, else None.
        """
        status, content = self.client.get_recommendations(user_id, n)
        if self.check_status(status, content):
            return content
        else:
            return None

    def fetch_user_ratings(self, user_id, movie_id):
        """
        Fetch the ratings given by a user.
        @param user_id: User ID
        @param movie_id: Movie ID, can be None
        @return: List of user ratings.
        """
        # Check if movie id is given.
        movie_id = None if movie_id == 0 else movie_id
        status, content = self.client.get_user_consumptions(user_id, movie_id)
        if self.check_status(status, content):
            return content
        else:
            return None

    def fetch_movie(self, movie_id):
        """
        Fetch information about a given movie.
        @param movie_id: Movie ID
        @return: Information about the movie.
        """
        status, content = self.client.get_item(movie_id)
        if self.check_status(status, content):
            return content
        else:
            return None

    def fetch_user(self, user_id):
        """
        Fetch information about a given user.
        @param user_id: User ID
        @return: Information about the user.
        """
        status, content = self.client.get_user(user_id)
        if self.check_status(status, content):
            return content
        else:
            return None

    def fetch_rating(self, consumption_id):
        """
        Fetch a specific rating of a movie, given a rating ID.
        @param consumption_id: Rating ID
        @return: The rating of a movie.
        """
        status, content = self.client.get_consumption(consumption_id)
        if self.check_status(status, content):
            return content
        else:
            return None

    ##########################################################################
    #                               DATA ADDITION                            #
    ##########################################################################

    def add_user(self, user_id):
        """
        Add a user with given ID to the Sugestio database.
        @param user_id: User ID
        """
        user = sugestio.User(id=user_id)
        self.client.add_user(user)

    def add_rating(self, user_id, movie_id, rating, timestamp):
        """
        Add a rating to the Sugestio database.
        @param user_id: User ID
        @param movie_id: Movie ID
        @param rating: Rating between 0.5 and 5
        @param timestamp: Unix time in UTC
        """
        consumption = sugestio.Consumption(userid=str(int(user_id)),
                                           itemid=str(int(movie_id)))
        consumption.type = "RATING"
        consumption.detail = "STAR:5:0.5:{}".format(rating)
        consumption.date = timestamp
        self.client.add_consumption(consumption)

    ##########################################################################
    #                                DATA REMOVAL                            #
    ##########################################################################

    def delete_user(self, user_id):
        """
        Delete user from Sugestio database.
        @param user_id: User ID
        """
        self.client.delete_user(user_id)

    def delete_user_ratings(self, user_id):
        """
        Delete all the ratings from one user.
        @param user_id: User ID
        """
        self.client.delete_user_consumptions(user_id)

    ##########################################################################
    #                                   HELPERS                              #
    ##########################################################################

    @staticmethod
    def check_status(status, content):
        """
        Check if the API request was successful. This happens when status
        is equal to 200.
        @param status: The status code.
        @param content: Response or error message.
        @return: True if request was success. Else False.
        """
        if status == 200:
            return True
        else:
            print('Librarian - Error performing API request.')
            print('Librarian - Server response code:', status)
            print('Librarian - Message: ', content)
            return False
