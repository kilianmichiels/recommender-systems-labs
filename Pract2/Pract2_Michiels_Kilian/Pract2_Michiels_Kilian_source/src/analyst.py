#!/usr/bin/env python
"""Analyst calculates some statistics about the data.
"""

# Authorship Information
__author__ = "Kilian Michiels"
__email__ = "Kilian.Michiels@UGent.be"
__date__ = "16/03/2019"


class Analyst(object):
    """
    Analyst performs some analysis on the given data such as finding averages,
    medians and positive/negative shares.
    """
    def __init__(self):
        super(Analyst, self).__init__()

    @staticmethod
    def analyse_data(data):
        """
        Analyse the given data. Calculate average rating, median rating and
        pos/neg/neutral share.
        @param data: DataFrame with the ratings
        """
        if len(data) == 0:
            print('Not enough data.')
            return
        print('\nAnalyst - Performing analysis...')
        print('--> Average: {}'.format(data['Rating'].mean()))
        print('--> Median: {}'.format(data['Rating'].median()))

        normalized_ratings = data['Rating'] - data['Rating'].mean()
        print('--> Positive Share: {:.2f}%'
              .format(100 * len(data[data['Rating'] > 3])
                      / len(normalized_ratings)))
        print('--> Negative Share: {:.2f}%'
              .format(100 * len(data[data['Rating'] < 3])
                      / len(normalized_ratings)))
        print('--> Neutral Share: {:.2f}%'
              .format(100 * len(data[data['Rating'] == 3])
                      / len(normalized_ratings)))
