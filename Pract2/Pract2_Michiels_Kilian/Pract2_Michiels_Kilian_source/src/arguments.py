#!/usr/bin/env python
"""Argument Parser for the application.
"""

# Imports
import argparse

# Authorship Information
__author__ = "Kilian Michiels"
__email__ = "Kilian.Michiels@UGent.be"
__date__ = "15/03/2019"


def fetch_args():
    """
    Fetch the arguments given by the user and convert these to a dictionary.
    @return: Dict: arguments
    """
    # Create parser
    parser = argparse.ArgumentParser(
        description="Recommender Systems - Practical Exercise 2: \
        Recommendations as a Service.",
        formatter_class=argparse.RawTextHelpFormatter)

    # Arguments
    parser.add_argument('-uid', '--user_id', type=int, default=1,
                        help="The user ID.")

    parser.add_argument('-mid', '--movie_id', type=int, default=None,
                        help="The movie ID.")

    parser.add_argument('-m', type=int, default=1,
                        help="Method that needs to be invoked:\n"
                             "1 = Fetch top N recommendations.\n"
                             "2 = Fetch user ratings.\n"
                             "3 = Add user with ratings.\n"
                             "4 = Add rating with given score.\n"
                             "5 = Fetch information about a given movie ID.")

    parser.add_argument('-n', type=int, default=5,
                        help="Number of movies with the highest score values "
                             "to show.")

    parser.add_argument('-s', type=float, default=2.5,
                        help="Score given to a movie.")

    parser.add_argument('-q', type=int, default=0,
                        help="Number of the question in the analysis to run.")

    parser.add_argument('--run_all', action='store_true', default=False,
                        help="Run main with all commands from PDF.")

    parser.add_argument('--start_with', type=int, default=1,
                        help="Start the Run All at given question.")

    parser.add_argument('-v', action="store_true", default=False,
                        help="Run the program with higher verbosity to see the "
                             "GET and POST URL requests.")

    parser.add_argument('--upload_data', action='store_true', default=False,
                        help="Upload the MovieLens data to Sugestio. Default: "
                             "False")

    args = vars(parser.parse_args())

    return args
